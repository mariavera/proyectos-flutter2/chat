import 'package:chat/src/widgets/custom_color_button.dart';
import 'package:chat/src/widgets/custom_input.dart';
import 'package:chat/src/widgets/labels.dart';
import 'package:chat/src/widgets/logo.dart';
import 'package:flutter/material.dart';

class RegisterPage extends StatelessWidget {
  const RegisterPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: const Color(0xffF2F2F2),
      body: SafeArea(
        child: CustomScrollView(
          physics: const BouncingScrollPhysics(),
          slivers: [
          SliverFillRemaining(
            hasScrollBody: false,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                const Logo(title: 'Registro',),
                _Form(),
                const Labels(route: 'register', text: '¿Ya tienes cuenta?', linkText: '¡Inicia sesión!',),
                const Padding(
                  padding: EdgeInsets.only(bottom: 20),
                  child: Text(
                    'Términos y condiciones de uso',
                    style: TextStyle(
                        fontWeight: FontWeight.w200, color: Colors.black54),
                  ),
                ),
              ],
            ),
          ),
        ]),
      ),
    );
  }
}

class _Form extends StatefulWidget {
  @override
  State<_Form> createState() => __FormState();
}

class __FormState extends State<_Form> {
  final emailCtrl = TextEditingController();
  final nameCtrl = TextEditingController();
  final passwordCtrl = TextEditingController();
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.only(top: 40),
      padding: const EdgeInsets.symmetric(horizontal: 50),
      child: Column(children: <Widget>[
        CustomInput(
          icon: Icons.perm_identity,
          placeHolder: 'Name',
          keyboardType: TextInputType.text,
          textController: nameCtrl,
        ),
          CustomInput(
          icon: Icons.mail_outline,
          placeHolder: 'Email',
          keyboardType: TextInputType.emailAddress,
          textController: emailCtrl,
        ),
        CustomInput(
          icon: Icons.lock_outline,
          placeHolder: 'Password',
          keyboardType: TextInputType.text,
          isPassword: true,
          textController: passwordCtrl,
        ),
       CustomColorButton(
        onPressed: (){print(emailCtrl.text + ',' + passwordCtrl.text);}, 
        text: 'Login')
      ]),
    );
  }
}
