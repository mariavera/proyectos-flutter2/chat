
import 'package:flutter/material.dart';

class Labels extends StatelessWidget {
  const Labels({Key? key, required this.route,required this.text,required this.linkText}) : super(key: key);

  final String route;
  final String text;
  final String linkText;

  @override
  Widget build(BuildContext context) {
    return Column(children: <Widget>[
       Text(
        text,
        style: const TextStyle(
            color: Colors.black45, fontSize: 14, fontWeight: FontWeight.w400),
      ),
      const SizedBox(
        height: 10,
      ),
      GestureDetector(
        onTap: (){ Navigator.pushReplacementNamed(context, route);},
        child:  Text(
          linkText,
          style: const TextStyle(
              color: Colors.blue, fontSize: 18, fontWeight: FontWeight.bold),
        ),
      ),
    ]);
  }
}
