import 'package:flutter/material.dart';

class CustomColorButton extends StatelessWidget {
  final void Function() onPressed;
  final Color color;
  final Color colorText;
  final String text;

  const CustomColorButton(
      {required this.onPressed,
      this.color = Colors.blue,
      this.colorText = Colors.white,
      required this.text,
      Key? key})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ElevatedButton(
        style: ButtonStyle(
          backgroundColor: MaterialStateProperty.resolveWith((_) => color),
          elevation: MaterialStateProperty.resolveWith(
              (state) => (state.contains(MaterialState.pressed)) ? 5 : 2),
          shape:
              MaterialStateProperty.resolveWith((_) => const StadiumBorder()),
        ),
        onPressed: onPressed,
        child: Container(
            height: 55,
            width: double.infinity,
            child: Center(
                child: Text(
              text,
              style: TextStyle(fontSize: 17, color: colorText),
            ))));
  }
}
