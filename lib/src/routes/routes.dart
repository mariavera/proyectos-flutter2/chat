

import 'package:chat/src/pages/chat_page.dart';
import 'package:chat/src/pages/loading_page.dart';
import 'package:chat/src/pages/login_page.dart';
import 'package:chat/src/pages/register_page.dart';
import 'package:chat/src/pages/users_page.dart';
import 'package:flutter/material.dart';

final Map<String, WidgetBuilder> appRoutes = {
  'users'   : ( _ ) => const UsersPage(),
  'chat'    : ( _ ) => const ChatPage(),
  'login'   : ( _ ) => const LoginPage(),
  'register': ( _ ) => const RegisterPage(),
  'loading' : ( _ ) => const LoadingPage(),

};